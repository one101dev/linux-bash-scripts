#!/bin/bash

# Uses script from http://code.google.com/p/android-apktool/wiki/BuildApktool?tm=4

build_folder=/home/derreck/apktools/build_experimental2

clear
echo "copying patch files to $HOME"
cp apktool_build1.patch ~/
cp apktool_build2.patch ~/
echo "creating directories in $build_folder"
mkdir -p $build_folder
cd $build_folder
echo "please wait... this will take several minutes"

# Download, build & install brut.j.common

git clone git://github.com/brutall/brut.j.common.git brut.j.common
cd brut.j.common
touch build.log
mvn install > build.log
cd ..

# Download, build & install brut.j.util

git clone git://github.com/brutall/brut.j.util.git brut.j.util
cd brut.j.util
touch build.log
mvn install > build.log
cd ..

# Download, build & install brut.j.dir

git clone git://github.com/brutall/brut.j.dir.git brut.j.dir
cd brut.j.dir
touch build.log
mvn install > build.log
cd ..

# Download, build & install brut.apktool.smali

git clone git://github.com/brutall/brut.apktool.smali.git brut.apktool.smali
cd brut.apktool.smali
touch build.log
mvn install > build.log
cd ..

# Download, build & install brut.apktool - will break, but patch fixes it

git clone git://github.com/brutall/brut.apktool.git brut.apktool
cd brut.apktool
touch build.log
mvn package > build.log

# Patch maven 1

echo "patching maven to build apktool (1 of 2)"
cd ~/
patch -p3 -i apktool_build1.patch

# Build & install brut.apktool - will break again, but patch fixes it

cd $build_folder/brut.apktool
touch build.log
mvn package > build.log

# Patch maven 2

echo "patching maven to build apktool (2 of 2)"
cd ~/
patch -p3 -i apktool_build2.patch

# Build & install brut.apktool - build success!

cd $build_folder/brut.apktool
touch build.log
mvn package > build.log
cd ..
echo "brut.apktool finished building"

# Build & install jf.smali-dev

echo "working on jf.smali-dev now"
git clone https://code.google.com/p/smali/ jf.smali-dev
cd jf.smali-dev
touch build.log
mvn package > build.log
cd ..
echo "jf.smali-dev finished building"
echo "check build.log in folders for status of all builds"
