#!/bin/bash

# Cleans up excess blank lines and malformed XML for all XML files in a given folder
# Usage
# put into res folder
# chmod a+x xmlparse.sh
# ./xmlparse.sh

for f in */*.xml;
do xmllint --pretty 1 --noblanks -o $f $f
done
